<?php

namespace Task\App\Controllers;

class Task extends \Task\Controller
{
    public function add()
    {
        $data = [
            'title' => 'Добавление задачи',
        ];

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $validate_error = $this->validateFields(
                [
                    'name' => [
                        'required' => true,
                        'message' => 'Поле Имя - не должно быть пустым',
                    ],
                    'email' => [
                        'required' => true,
                        'email' => true,
                        'message' => 'Поле E-mail - не должно быть пустым и содержать правильный e-mail',
                    ],
                    'text' => [
                        'required' => true,
                        'message' => 'Поле Текст задачи - не должно быть пустым',
                    ],
                ]
            );

            if ($validate_error === false) {
                $task_data = [
                    'user_name' => filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS),
                    'user_email' => filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS),
                    'text' => filter_input(INPUT_POST, 'text', FILTER_SANITIZE_SPECIAL_CHARS),
                    'status' => 0,
                ];

                $model = $this->loadModel('tasks');
                $model->insert($task_data);

                $this->setAlert('Успешно добавили новую задачу.');

                header('Location:' . BASE_URL);
            }
            $data['error'] = implode('<br>', $validate_error);
        }

        $this->view->show('task_add', $data);
    }

    public function edit($params)
    {
        if (!IS_ADMIN) {
            $this->setAlert('Только администратор может редактировать задачи.', 'alert-danger');
            header('Location:' . BASE_URL);
        }

        $data = [
            'title' => 'Редактирование задачи',
        ];

        $id = null;

        if (count($params)) {
            $id = intval($params[0]);
        }

        $model = $this->loadModel('tasks');
        $task_data = $model->getById($id);

        if (!$task_data) {
            header('Location:' . BASE_URL);
        }

        $data['task'] = $task_data;

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $new_task_data = [
                'text' => filter_input(INPUT_POST, 'text', FILTER_SANITIZE_SPECIAL_CHARS),
                'status' => intval(filter_input(INPUT_POST, 'status', FILTER_SANITIZE_SPECIAL_CHARS)),
            ];

            if ($task_data->text !== $new_task_data['text']) {
                $new_task_data['changed'] = 1;
            }

            $model->update($id, $new_task_data);
            header('Location:' . BASE_URL);
        }

        $this->view->show('task_edit', $data);
    }
}
