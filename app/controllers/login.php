<?php

namespace Task\App\Controllers;

class Login extends \Task\Controller
{
    public function index($params = [])
    {
        $data = [
            'title' => 'Вход',
        ];

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $validate_error = $this->validateFields(
                [
                    'login' => [
                        'required' => true,
                        'message' => 'Поле Имя - не должно быть пустым',
                    ],
                    'password' => [
                        'required' => true,
                        'message' => 'Поле Пароль - не должно быть пустым',
                    ],
                ]
            );

            $login_data = [
                'login' => filter_input(INPUT_POST, 'login', FILTER_SANITIZE_SPECIAL_CHARS),
                'password' => filter_input(INPUT_POST, 'password', FILTER_SANITIZE_SPECIAL_CHARS),
            ];

            if ($validate_error === false) {
                if (ADMIN['login'] == $login_data['login'] and ADMIN['password'] == $login_data['password']) {
                    $_SESSION['logged_in'] = true;
                    header('Location:' . BASE_URL);
                } else {
                    $data['error'] = 'Неверный логин или пароль';
                }
            } else {
                $data['error'] = implode('<br>', $validate_error);
            }
        }

        $this->view->show('login', $data);
    }

    public function logout()
    {
        unset($_SESSION['logged_in']);
        header('Location:' . BASE_URL);
    }
}
