<?php

namespace Task\App\Controllers;

class Main extends \Task\Controller
{
    public function index($params = [])
    {
        $max = 3;
        $page = isset($params[0]) ? intval($params[0]) : 0;
        $page = ($page < 2) ? 0 : $page - 1;
        $page_offset = $page * $max;

        $sort_field = isset($_SESSION['sort_field']) ? $_SESSION['sort_field'] : 'name';
        $sort_dir = isset($_SESSION['sort_dir']) ? $_SESSION['sort_dir'] : 'asc';

        $sort_field_names = [
            'name' => 'user_name',
            'email' => 'user_email',
            'status' => 'status',
        ];
        $sort_field_dir_names = [
            'asc' => ' ASC',
            'desc' => ' DESC',
        ];

        $order_by = $sort_field_names[$sort_field] . $sort_field_dir_names[$sort_dir];

        $data = [
            'title' => 'Список задач',
            'order' => [
                'name' => $sort_field,
                'dir' => $sort_dir,
            ],
        ];

        $model = $this->loadModel('tasks');
        $data['tasks'] = $model->getAll($max, $page_offset, $order_by);

        $tasks_count = $model->getCount();
        $paginate = [
            'total' => $tasks_count,
            'total_pages' => (intdiv($tasks_count, $max) + (($tasks_count % $max) ? 1 : 0)),
            'page' => $page,
        ];
        $data['paginate'] = $paginate;

        $this->getAlert();
        if ($this->alert) {
            $data['alert'] = $this->alert;
        }

        $this->view->show('main', $data);
    }

    public function order($params = [])
    {
        $def_field = 'name';
        $def_dir = 'asc';
        $sort_params = ['name', 'email', 'status'];
        $sort_dir = ['asc', 'desc'];

        if (isset($params[0]) and (in_array($params[0], $sort_params))) {
            $def_field = $params[0];
        }
        if (isset($params[1]) and (in_array($params[1], $sort_dir))) {
            $def_dir = $params[1];
        }

        $_SESSION['sort_field'] = $def_field;
        $_SESSION['sort_dir'] = $def_dir;

        header('Location:' . BASE_URL);
    }
}
