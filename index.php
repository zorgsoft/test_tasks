<?php
session_start();
ini_set('display_errors', 1);

define('DS', DIRECTORY_SEPARATOR);
define(
    'BASE_URL',
    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] .
        rtrim(dirname($_SERVER['SCRIPT_NAME']), '/\\')
);

define('HOME', dirname(__FILE__) . DS);
define('CORE', HOME . 'core' . DS);
define('APP', HOME . 'app' . DS);

define(
    'ADMIN',
    [
        'login' => 'admin',
        'password' => '123',
    ]
);

define(
    'DB',
    [
        'host' => 'localhost',
        'name' => 'test_tasks',
        'user_name' => 'root',
        'password' => '',
    ]
);

require_once APP . '/bootstrap.php';

$options = [
    'default_route' => 'main',
    'default_action' => 'index',
];

$app = new Task\Core($options);
define('IS_ADMIN', $app->isAdmin());
$app->start();
