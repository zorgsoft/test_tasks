<?php
namespace Task;

class View
{
    private $templates_dir = APP . 'views' . DS;

    public function show($template, $data = array(), $show = true)
    {
        $return = '';
        $template = $this->templates_dir . $template . '.php';

        if (file_exists($template)) {
            extract($data);
            ob_start();
            include $template;
            $return_str = ob_get_contents();
            ob_end_clean();
        }

        if (!$show) {
            return $return_str;
        }

        echo $return_str;
    }
}
