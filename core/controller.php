<?php

namespace Task;

class Controller
{
    public $view;
    public $alert = null;

    public function __construct()
    {
        $this->view = new View();
    }

    public function index($params = [])
    {
        return;
    }

    public function loadModel($model_name, $model_table = null)
    {
        $model_file = strtolower($model_name) . '.php';
        $model_path = APP . "models/" . $model_file;

        $model_table = $model_table ? strtolower($model_table) : strtolower($model_name);

        if (file_exists($model_path)) {
            include $model_path;
        }

        $model_name = 'Task\\App\\Models\\' . ucfirst($model_name);
        if (class_exists($model_name)) {
            return new $model_name($model_table);
        } else {
            die('Model load error: ' . $model_name);
        }
        return null;
    }

    public function validateFields($fields = [])
    {
        $error = false;
        foreach ($fields as $field_name => $field) {
            $field_error = false;
            foreach ($field as $key => $val) {
                switch ($key) {
                    case 'required':
                        if (!isset($_POST[$field_name]) or empty($_POST[$field_name])) {
                            $field_error = true;
                        }
                        break;
                    case 'email':
                        if (!isset($_POST[$field_name]) or filter_var($_POST[$field_name], FILTER_VALIDATE_EMAIL) === false) {
                            $field_error = true;
                        }
                        break;
                }
            }

            if ($field_error and isset($fields[$field_name]['message'])) {
                $error[] = $fields[$field_name]['message'];
            }
        }

        return $error;
    }

    public function setAlert($message, $class = 'alert-success')
    {
        $_SESSION['alert_text'] = $message;
        $_SESSION['alert_class'] = $class;
    }

    public function getAlert()
    {
        if (isset($_SESSION['alert_text'])) {
            $this->alert = [
                'message' => $_SESSION['alert_text'],
                'class' => $_SESSION['alert_class'],
            ];
        }

        unset($_SESSION['alert_text']);
        unset($_SESSION['alert_class']);

        return $this->alert;
    }
}
