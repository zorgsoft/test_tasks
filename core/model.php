<?php

namespace Task;

class Model
{
    private $options;
    public $table = '';
    public $db = null;

    public function __construct($table)
    {
        setLocale(LC_ALL, 'ru_RU.UTF-8');
        $this->options = DB;
        $this->table = $table;

        try {
            $this->db = new \PDO(
                'mysql:host=' . $this->options['host'] .
                    ';dbname=' . $this->options['name'] .
                    ';charset=UTF8',
                $this->options['user_name'],
                $this->options['password']
            );
        } catch (\PDOException $e) {
            die('DB Error: [' . $e->getCode() . '] ' . $e->getMessage());
        }
    }

    public function insert($data)
    {
        $keys = array_keys($data);
        $fields = implode(",", $keys);
        $placeholders = str_repeat('?,', count($keys) - 1) . '?';
        $query = 'INSERT INTO `' . $this->table . '` (' . $fields . ') VALUES (' . $placeholders . ')';

        return $this->db->prepare($query)->execute(array_values($data));
    }

    public function update($id, $data)
    {
        $sql_set = [];
        foreach ($data as $key => $val) {
            $sql_set[] = $key . '=:' . $key;
        }
        $data['id'] = $id;
        $query = 'UPDATE `' . $this->table . '` SET ' . implode($sql_set, ', ') . ' WHERE id = :id';

        return $this->db->prepare($query)->execute($data);
    }

    public function getById($id)
    {
        $query = 'SELECT * FROM `' . $this->table . '` WHERE id = :id';

        $result = $this->db->prepare($query);
        $result->bindParam(':id', $id, \PDO::PARAM_INT);
        $result->setFetchMode(\PDO::FETCH_OBJ);

        $result->execute();

        return $result->fetch();
    }

    public function getAll($limit = null, $offset = null, $order_by = null)
    {
        $limit = $limit ? ' LIMIT ' . intval($limit) . ' ' : '';
        $offset = $offset ? ' OFFSET ' . intval($offset) . ' ' : '';
        $order_by = $order_by ? ' ORDER BY ' . $order_by . ' ' : '';
        // echo $order_by;
        // die();
        $query = 'SELECT * FROM `' . $this->table . '` ' . $order_by . $limit . $offset;

        $result = $this->db->prepare($query);
        $result->setFetchMode(\PDO::FETCH_OBJ);

        $result->execute();

        return $result->fetchAll();
    }

    public function deleteById($id)
    {
        $query = 'DELETE FROM `' . $this->table . '` WHERE id = :id';

        $result = $this->db->prepare($query);
        $result->bindValue(':id', $id);

        $result->execute();

        return $result->rowCount();
    }

    public function getCount()
    {
        return $this->db->query('SELECT count(*) FROM `' . $this->table . '`')->fetchColumn();
    }
}
