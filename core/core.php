<?php
namespace Task;

class Core
{
    private $options;
    public $url;

    public function __construct($options)
    {
        $this->options = $options;

        $this->url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .
            '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

    public function start()
    {
        $this->route();
    }

    private function route()
    {
        $controller_name = $this->options['default_route'];
        $action_name = $this->options['default_action'];
        $params = [];

        $routes = explode('/', substr($this->url, mb_strlen(BASE_URL) + 1));

        if (!empty($routes[0])) {
            $controller_name = $routes[0];
        }

        if (!empty($routes[1])) {
            $action_name = mb_strtolower($routes[1]);
        }

        if (count($routes) > 2) {
            $params = array_slice($routes, 2);
        }

        $controller_file = strtolower($controller_name) . '.php';
        $controller_path = APP . "controllers/" . $controller_file;

        if (file_exists($controller_path)) {
            include $controller_path;
        } else {
            $this->error404('Controller ' . $controller_name . ' not found');
            return;
        }

        $controller_name = 'Task\\App\\Controllers\\' . ucfirst($controller_name);
        $controller = new $controller_name;
        $action = $action_name;

        if (method_exists($controller, $action)) {
            $controller->$action($params);
        } else {
            $this->error404('Method ' . $action . ' not found in ' . $controller_name);
        }
    }

    public function error404($message = '')
    {
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        echo 'Error: 404<hr>';
        if ($message) {
            echo $message;
        }
    }

    public function isAdmin()
    {
        return isset($_SESSION['logged_in']) ? true : false;
    }
}
